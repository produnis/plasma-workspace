# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-12-20 21:19+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.4\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"

#: package/contents/ui/BrightnessItem.qml:71
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Brightness and Color"
msgstr "בהירות וצבע"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Screen brightness at %1%"
msgstr "בהירות המסך עומדת על %1%"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "בהירות המקלדת עומדת על %1%"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "תאורת לילה כבויה"

#: package/contents/ui/main.qml:107
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "תאורת לילה עומדת על %1K"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "גלילה תכוון את בהירות המסך"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "לחיצה אמצעית תחליף את מצב תאורת הלילה"

#: package/contents/ui/main.qml:258
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "הגדרת תאורת לילה…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "כבוי"

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "לא זמין"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "לא פעיל"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "לא רץ"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "פעיל"

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "מעבר בוקר"

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "יום"

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "מעבר ערב"

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "לילה"

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "הגדרה…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "הפעלה והגדרה…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "המעבר ליום צריך להסתיים עד:"

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "המעבר ללילה מתוזמן לשעה:"

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "המעבר ללילה צריך להסתיים עד:"

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "המעבר ליום מתוזמן לשעה:"

#: package/contents/ui/PopupDialog.qml:70
#, kde-format
msgid "Display Brightness"
msgstr "בהירות תצוגה"

#: package/contents/ui/PopupDialog.qml:101
#, kde-format
msgid "Keyboard Brightness"
msgstr "בהירות מקלדת"

#: package/contents/ui/PopupDialog.qml:132
#, kde-format
msgid "Night Light"
msgstr "תאורת לילה"
