# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jure Repinc <jlp@holodeck1.com>, 2009, 2024.
# Andrej Mernik <andrejm@ubuntu.si>, 2013, 2014.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-13 00:39+0000\n"
"PO-Revision-Date: 2024-02-20 02:23+0100\n"
"Last-Translator: Jure Repinc <jlp@holodeck1.com>\n"
"Language-Team: LUGOS\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Lokalize 23.11.70\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: menu.cpp:102
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "Prikaži KRunner"

#: menu.cpp:107
#, kde-format
msgid "Open Terminal"
msgstr "Odpri terminal"

#: menu.cpp:111
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Zakleni zaslon"

#: menu.cpp:120
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "Zapusti …"

#: menu.cpp:125
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr "Nastavitve zaslona …"

#: menu.cpp:285
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Nastavi vstavek za vsebinske menije"

#: menu.cpp:295
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[ostala dejanja]"

#: menu.cpp:298
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Dejanja za ozadje"

#: menu.cpp:302
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[ločilnik]"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "Zaženi ukaz ..."
